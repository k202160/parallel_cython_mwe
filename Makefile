all: main

CC=mpicc
PYTHON=python
CFLAGS := $(shell python3-config --cflags) -fPIC

main: main.o buffer_module.o buffer_lib.o
	$(CC) $^ $(shell python3-config --embed --ldflags) -o $@

buffer_module.o: buffer_module.c
	$(CC) $(shell python3-config --cflags) -c $^ -o $@

buffer_module.c: buffer_module.pyx
	$(PYTHON) -m cython -3 $<

clean:
	rm -rf *.o buffer_module.c main
