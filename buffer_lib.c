#include <assert.h>
#include <mpi.h>
#include "buffer_lib.h"

#define SIZE 16
double buffer[SIZE];

void set_buffer(double* data, size_t size){
  assert(size <= SIZE);
  for(size_t i=0; i<size;++i)
    buffer[i] = data[i];
}

double* get_buffer(){
  return &buffer[0];
}

size_t get_size(){
  return SIZE;
}

int get_rank(){
  int rank;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  return rank;
}
