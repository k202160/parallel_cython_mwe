#include <stddef.h>

void set_buffer(double* data, size_t size);
double* get_buffer();
size_t get_size();
int get_rank();
