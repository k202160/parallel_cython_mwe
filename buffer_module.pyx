cimport buffer_lib

import numpy as _np
cimport numpy as _np

def set_buffer(data):
    cdef double[:] data_c = _np.ascontiguousarray(data, dtype=_np.double)
    buffer_lib.set_buffer(&data_c[0], len(data_c))

def get_buffer():
    cdef double* ptr = buffer_lib.get_buffer()
    cdef size_t size = buffer_lib.get_size()
    return <double[:size]>(ptr)

def get_rank():
    return buffer_lib.get_rank()
