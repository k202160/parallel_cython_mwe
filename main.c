#include <stdio.h>
#include <mpi.h>
#include <Python.h>

extern PyObject *(PyInit_buffer_module)(void);

int main(int argc, char** argv){
  if (argc < 2){
    fprintf(stderr, "Please pass the file name of a python script as command-line argument.");
    return -1;
  }
  MPI_Init(&argc, &argv);
  PyImport_AppendInittab("buffer_module", &PyInit_buffer_module);
  Py_Initialize();
  FILE* file = fopen(argv[1], "r");
  if (file == NULL){
    fprintf(stderr, "Error: Cannot read %s\n", argv[1]);
    return -1;
  }
  PyObject* globals = PyDict_New();
  PyObject* pyResult = PyRun_File(file, argv[1], Py_file_input, globals, globals);
  Py_XDECREF(pyResult);
  fclose(file);
  if (PyErr_Occurred()){
    PyErr_Print();
    fprintf(stderr, "Error while executing file %s\n", argv[1]);
    return -1;
  }
  Py_Finalize();
  MPI_Finalize();
  return 0;
}
