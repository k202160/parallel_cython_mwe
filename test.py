import buffer_module
import numpy as np

rank = buffer_module.get_rank()
buffer_module.set_buffer([i**rank for i in range(16)])

b = buffer_module.get_buffer()
print(rank, b)
print(rank, np.asarray(b))
